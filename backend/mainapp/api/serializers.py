from rest_framework import serializers 

from mainapp.models import Blogs, Kategorite, Challenges, Comments_challenges, Comments_blogs

class BlogsSerializer(serializers.ModelSerializer):
    class Meta: 
        model = Blogs
        fields =('id','kategoria','titulli','permbajtja','data','foto','admin_id')

class KategoriteSerializer(serializers.ModelSerializer):
    class Meta: 
        model = Kategorite
        fields =('id','emri')

class ChallengesSerializer(serializers.ModelSerializer):
    class Meta: 
        model = Challenges
        fields =('id','titulli','text','foto','file','data','expire_date')

class Comments_challengesSerializer(serializers.ModelSerializer):
    class Meta: 
        model = Comments_challenges
        fields =('id','user_id','Challenges_id','text','data','file')

class Comments_blogsSerializer(serializers.ModelSerializer):
    class Meta: 
        model = Comments_blogs
        fields =('id','user_id','Blogs_id','text','data')
