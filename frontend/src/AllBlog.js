import React from 'react';
import ReactDOM from 'react-dom';

class AllBlog extends React.Component{
    render() {
      return (
        <div>
          <title>Blog</title>
          {/* Latest compiled and minified CSS */}
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css" />
          {/* jQuery library */}
          {/* Latest compiled JavaScript */}
          <h1>ALL BLOGS</h1>
          <hr style={{borderBottom: 'solid black 3px'}} />
          <div className="col-md-12 d-lg-none" style={{margin: '2%'}}>
            <div className="col-md-8">
              <div className="col-md-12">
                <h2>Lajmi 1</h2>
                <hr style={{borderBottom: 'solid black 2px'}} />
                <p>Date &amp; Time | Posted by ***</p>
                <table>
                  <tbody><tr>
                      <td rowSpan={2} ><img src="New.png" height={200} /></td>
                      <td><span>Description</span></td>
                    </tr>
                    <tr>
                      <td><button className="readMore btn btn-warning">Read More ...</button></td>
                    </tr>
                  </tbody></table>
              </div>
              <div className="col-md-12">
                <h2>Lajmi 2</h2>
                <hr style={{borderBottom: 'solid black 2px'}} />
                <p>Date &amp; Time | Posted by ***</p>
                <table>
                  <tbody><tr>
                      <td rowSpan={2} ><img src="New.png" height={200} /> </td>
                      <td><span>Description</span></td>
                    </tr>
                    <tr>
                      <td><button className="readMore btn btn-warning">Read More ...</button></td>
                    </tr>
                  </tbody></table>
              </div>
            </div>
            <div className="col-md-4">
              <img src="add.png" height={300} />
            </div>
          </div>
          <button className="pull-left btn btn-default" style={{backgroundColor: '#515145'}}>Previous Page</button>
          <button className="pull-right btn btn-default" style={{backgroundColor: '#515145'}}>Next Page</button>
        </div>
      );
    }
  }
 
export default AllBlog;